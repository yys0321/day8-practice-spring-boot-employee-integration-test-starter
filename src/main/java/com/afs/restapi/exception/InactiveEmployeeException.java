package com.afs.restapi.exception;

public class InactiveEmployeeException extends RuntimeException {
    public InactiveEmployeeException() {
        super("Employee is inactive");
    }
}
