package com.afs.restapi.service;

import com.afs.restapi.Employee;
import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public Employee create(Employee newEmployee) {
        if (newEmployee.getAge() < 18 || newEmployee.getAge() > 65) {
            throw new InvalidEmployeeException();
        }
        return employeeRepository.insert(newEmployee);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = findById(id);
        if (!employeeToUpdate.isActiveStatus())
            throw new InactiveEmployeeException();
        return employeeRepository.update(id, employee);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public void delete(Long id) {
        employeeRepository.delete(id);
    }
}
