package com.afs.restapi;

import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc

public class EmployeeControllerTest {

    @Autowired
    MockMvc postmanMock;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setup() {
        employeeRepository.reset();
    }

    @Test
    void should_return_all_employees_when_perform_get_given_employees() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(5)));
    }

    @Test
    void should_return_employee_when_perform_get_given_employee_id() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("John Smith"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(32))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
    }

    @Test
    void should_change_active_status_to_false_when_perform_delete_given_employee_id() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.delete("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        assertFalse(employeeRepository.findById(1L).isActiveStatus());
    }

    @Test
    void should_return_male_employee_when_perform_get_given_employee_gender() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("gender", "Male");
        postmanMock.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$..gender").value(everyItem(is("Male"))));
    }

    @Test
    void should_return_right_number_of_employees_when_perform_get_given_employees_page_num_page_size() throws Exception {
        List<Employee> employeeListMock = new ArrayList<>();
        employeeListMock.add(new Employee(3L, "David Williams", 35, "Male", 5500));
        employeeListMock.add(new Employee(4L, "Emily Brown", 23, "Female", 4500));
        String employeeListMockString = objectMapper.writeValueAsString(employeeListMock);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("pageNumber", String.valueOf(2))
                .param("pageSize", String.valueOf(2));
        postmanMock.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(result -> assertEquals(employeeListMockString, result.getResponse().getContentAsString()));

    }

    @Test
    void should_return_new_employee_when_perform_post_given_new_employee() throws Exception {
        employeeRepository.clearAll();

        String newEmployeeJson = "{\n" +
                " \"name\": \"Lisa 10\",\n" +
                " \"age\": 22,\n" +
                " \"gender\": \"Female\",\n" +
                " \"salary\": 9000\n" +
                "}";

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newEmployeeJson);

        postmanMock.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lisa 10"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());

        List<Employee> all = employeeRepository.findAll();
        assertEquals(1, all.size());
        assertEquals("Lisa 10", all.get(0).getName());
        assertNotNull(all.get(0).getId());
    }

    @Test
    void should_return_updated_employee_when_perform_put_given_age_and_salary() throws Exception {
        String expect = objectMapper.writeValueAsString(new Employee(1L, "John Smith", 20, "Male", 1000));
        String newEmployeeJson = "{\n" +
                "\"age\": 20,\n" +
                "\"salary\": 1000\n" +
                "}";
        postmanMock.perform(MockMvcRequestBuilders.put("/employees/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_404_when_perform_get_by_id_given_employee_not_exist() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.get("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_404_when_perform_delete_given_employee_not_exist() throws Exception {
        postmanMock.perform(MockMvcRequestBuilders.delete("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_return_404_when_perform_update_given_employee_not_exist() throws Exception {
        String newEmployeeJson = "{\n" +
                " \"name\": \"Lisa 10\",\n" +
                " \"salary\": 9000\n" +
                "}";
        postmanMock.perform(MockMvcRequestBuilders.put("/employees/999")
                        .content(newEmployeeJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
