package com.afs.restapi;

import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)

public class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;
    @InjectMocks
    private EmployeeService employeeService;

    @Test
    void should_throw_invalid_employee_when_create_given_employee_age_less_than_18() {
        Employee employee = new Employee("Tom", 16, "Male", 1000);

        String exceptionMessage = assertThrows(InvalidEmployeeException.class, () -> {
            employeeService.create(employee);
        }).getMessage();
        assertEquals("Employee must be 18-65 years old", exceptionMessage);
    }

    @Test
    void should_throw_invalid_employee_when_create_given_employee_age_larger_than_65() {
        Employee employee = new Employee("May", 70, "Female", 10000);

        String exceptionMessage = assertThrows(InvalidEmployeeException.class, () -> {
            employeeService.create(employee);
        }).getMessage();
        assertEquals("Employee must be 18-65 years old", exceptionMessage);
    }

    @Test
    void should_add_new_employee_when_create_given_employee_age_in_range() {
        Employee employee = new Employee("John", 30, "Male", 10000);
        employeeService.create(employee);
        verify(employeeRepository, times(1)).insert(employee);
    }

    @Test
    void should_throw_inactive_employee_when_update_given_inactive_employee() {
        Employee oldEmployeeData = new Employee(1L, "Kelly", 25, "Female", 5000);
        Employee newEmployeeData = new Employee("Kelly", 30, "Female", 10000);

        when(employeeRepository.findById(1L)).thenReturn(oldEmployeeData);
        Employee targetEmployee = employeeRepository.findById(1L);
        targetEmployee.setActiveStatus(false);

        String exceptionMessage = assertThrows(InactiveEmployeeException.class, () -> {
            employeeService.update(1L, newEmployeeData);
        }).getMessage();
        assertEquals("Employee is inactive", exceptionMessage);
    }

    @Test
    void should_update_employee_data_when_update_given_employee() {
        Employee oldEmployeeData = new Employee(1L, "Kelly", 25, "Female", 5000);
        Employee newEmployeeData = new Employee("Karry", 30, "Female", 10000);

        when(employeeRepository.findById(1L)).thenReturn(oldEmployeeData);
        employeeService.update(1L, newEmployeeData);
        verify(employeeRepository, times(1)).update(1L, newEmployeeData);
    }

    @Test
    void should_find_all_employees_when_find_all_given_employees() {
        employeeService.findAll();
        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void should_find_specific_employee_when_find_by_id_given_employee_id() {
        Long id = 1L;
        employeeService.findById(id);
        verify(employeeRepository, times(1)).findById(id);
    }

    @Test
    void should_find_male_employees_when_find_by_gender_given_employee_gender() {
        String gender = "male";
        employeeService.findByGender(gender);
        verify(employeeRepository, times(1)).findByGender(gender);
    }

    @Test
    void should_find_employees_in_specific_page_when_find_by_page_given_page_number_and_page_size() {
        int pageNumber = 1;
        int pageSize = 10;
        employeeService.findByPage(pageNumber, pageSize);
        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    void should_delete_specific_employee_when_delete_given_employee_id() {
        Long id = 1L;
        employeeService.delete(id);
        verify(employeeRepository, times(1)).delete(id);
    }
}
